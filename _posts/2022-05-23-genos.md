---
title: Προτεινόμενα γένη στα ελληνικά για λογισμικά και υλισμικά (S/W, H/W)
author: Greek Raspberry Team
date: 2022-05-23
category: gender
layout: post
---

Χρησιμοποιείτε τα παρακάτω γένη.

Όπου συναντάτε scratch, unity, python, θα μεταφράζετε **η scratch**, **η unity**, **η python**

Όπου συναντάτε Api, app inventor, arduino, θα μεταφράζετε **το Api**, **το app inventor**, **το arduino**
